#!/bin/bash
###
#Defaultls
chiffrement="ed25519"
bits="4096"
passPhrase=""

directory=$HOME'/.ssh/'
fileName=$directory"id_"$chiffrement


sshUsername='notroot'
sshHost='192.168.0.45'
sshPort='2501'

###
#Gestion des Paramêtres passés
#EN COURS DE REDACTION
for param in "$@"
do
 case $param in
   "--username" | "-u")
               ;;
   "--host" | "-h")
               ;;
   "--port" | "-p")
              ;;
   "--chiffrement" | "-c")
              ;;
   "--bits" | "-b")
               ;;
   "--output" | "-o")
              ;;
   "--passphrase" | "-P")
               ;;
   "--help" | "-h")
              l ;;
   *);;

 esac
done

sshLogin="$sshUsername@$sshHost -p $sshPort"

echo 'Suprresion des clés temporaires précedentes'
rm $fileName".tmp" $fileName".tmp.pub" || true

echo 'Géneration de la nouvelle clé'
ssh-keygen -t $chiffrement -b $bits -f $fileName".tmp" -N "$passPhrase" || exit

echo 'Copie sur le serveur de  la nouvelle clé'
ssh-copy-id -i $fileName".tmp.pub" $sshLogin || exit

echo "Renommage de l'ancienne clé $fileName.pub et "$fileName
mv $fileName".pub" $fileName".pub.old" && mv $fileName $fileName".old"

echo 'Installation de la nouvelle clé '$fileName".tmp.pub et $fileName.tmp"
mv $fileName".tmp.pub" $fileName".pub" && mv $fileName".tmp" $fileName

#Exit en cas d'erreur donc ne supprime pas les anciennes clés
echo 'test de Connexion'
ssh $sshLogin echo "OK" || exit
#ssh $sshLogin sed -i -e "s/$(cat $fileName".pub.old")//g" ~/.ssh/autorized_keys
#ssh $sshLogin sed -i -e "s/$(cat $fileName".pub.old")//g" ~/.ssh/autorized_keys2

echo 'Suppression de vielle clé'
rm $fileName".pub.old" $fileName".old"
